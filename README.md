# Woohoo Generator

`yo woohoo` will scaffold a new woohoo application(s) for you, and offer to set up other dependent application(s)

## Getting Started

First up, you'll need Node.js >= 6.0 If you don't have them, follow the **Dependencies** instructions below.

Then, install the Woohoo generator:

```sh
$ git clone https://<your username>@bitbucket.org/ivanclaud/woohoo-generator.git
$ cd woohoo-generator
$ npm install
$ npm link
```

To update Woohoo generator:
```sh
$ cd woohoo-generator
$ git pull
```

With the generator installed, create an empty directory for your workspace, and run `yo woohoo` in it:

```sh
$ yo woohoo
```

The generator will ask you a few questions about which application to include and it's configurations

### What do you mean 'yo: command not found'?

When running 'yo woohoo', you'll run into this problem if you don't have [Yeoman](http://yeoman.io/) installed. Yeoman is a generator ecosystem, install it one go.

```sh
$ npm install -g yo
```
For more information, see the [Yeoman Getting Started Page](http://yeoman.io/learning/index.html).

## Dependencies

### Install Node.js

Download and install the node.js binaries for your platform from the [Node.js download page](http://nodejs.org/download/).

### Environment/PATH Variables

Woohoo B2B server expectes following Dependencies in environment/PATH Variables

*   PHP
*   MYSQL
*	GIT

## License

Copyright (c) 2006 - 2018 Qwikcilver Solutions Pvt Ltd. All rights reserved. ISO 27001:2013 certified