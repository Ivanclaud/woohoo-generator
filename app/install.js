/**
 * Installs all application based on configuration
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	return new Promise((resolve, reject) => {
		require('./install/b2bserver')(yeoman)
			.then(() => {
				return require('./install/b2bwebui')(yeoman);
			})
			.then(() => {
				return resolve();
			})
			.catch(err => {
				return reject(err);
			});
	});
};