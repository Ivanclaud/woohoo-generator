/**
 * Prepare file system for all application
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	return new Promise((resolve, reject) => {
		require('./files/b2bserver')(yeoman)
			.then(() => {
				return require('./files/b2bwebui')(yeoman);
			})
			.then(() => {
				return resolve();
			})
			.catch(err => {
				return reject(err);
			});
	});
};