/**
 * Prepares woohoo B2B web UI file system
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	/**
	 * Clone b2b-web-ui repo using given  username & branch
	 * 
	 * @return {Promise}
	 */
	let cloneRepo = () => {
		let url = 'https://' + yeoman.userInputs.b2bwebui.bitbucketUsername + '@bitbucket.org/qwikteam/b2b-web-ui.git',
			bat;
		
		yeoman.log("Cloning `%s` branch from url `%s` on folder `%s`", yeoman.userInputs.b2bwebui.branch, url, yeoman.userInputs.b2bwebui.folder);
		
		bat = yeoman.spawnCommandSync('git', [
			'clone',
			'-b',
			yeoman.userInputs.b2bwebui.branch,
			url,
			yeoman.userInputs.b2bwebui.folder
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Create woohoo custom configuration file
	 * Which contains oauth details
	 * 
	 * @return {Function} cb
	 */
	let createProxyConfigXml = () => {
		let cb = yeoman.async();
		yeoman.fs.copyTpl(
			yeoman.templatePath('b2bwebui/proxy/data/config.xml'),
			yeoman.destinationPath(yeoman.userInputs.b2bwebui.folder + '/proxy/data/config.xml'),
			yeoman.userInputs.b2bwebui
		);
		return cb();
	}

	return new Promise((resolve, reject) => {
		if(yeoman.userInputs.b2bwebui == false) {
			return resolve();
		}

		cloneRepo()
			.then(() => {
				return createProxyConfigXml();
			})
			.then(() => {
				return resolve()
			})
			.then(err => {
				return reject(err);
			});
	});

}