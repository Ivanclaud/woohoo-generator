const _ = require('lodash');

/**
 * Prepares woohoo B2B server file system
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	/**
	 * Clone b2b-magento-server repo using given  username & branch
	 * 
	 * @return {Promise}
	 */
	let cloneRepo = () => {
		let url = 'https://' + yeoman.userInputs.b2bserver.bitbucketUsername + '@bitbucket.org/qwikteam/b2b-magento-server.git',
			bat;
		
		yeoman.log("Cloning `%s` branch from url `%s` on folder `%s`", yeoman.userInputs.b2bserver.branch, url, yeoman.userInputs.b2bserver.folder);
		
		bat = yeoman.spawnCommandSync('git', [
			'clone',
			'-b',
			yeoman.userInputs.b2bserver.branch,
			url,
			yeoman.userInputs.b2bserver.folder
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Create magento database configuration file
	 * 
	 * @return {Function} cb
	 */
	let createLocalXml = () => {
		let cb = yeoman.async();
		yeoman.fs.copyTpl(
			yeoman.templatePath('b2bserver/app/etc/local.xml'),
			yeoman.destinationPath(yeoman.userInputs.b2bserver.folder + '/app/etc/local.xml'),
			_.merge(yeoman.userInputs.b2bserver, {
				installDate: new Date().toUTCString()
			})
		);
		return cb();
	}

	/**
	 * Create woohoo custom configuration file
	 * Which contains mongo db connection details
	 * 
	 * @return {Promise}
	 */
	let createGBConfigXml = () => {
		let cb = yeoman.async();
		yeoman.fs.copyTpl(
			yeoman.templatePath('b2bserver/app/etc/gbConfig.xml'),
			yeoman.destinationPath(yeoman.userInputs.b2bserver.folder + '/app/etc/gbConfig.xml'),
			yeoman.userInputs.b2bserver
		);
		return cb();
	}

	/**
	 * Create behat custom configuration file
	 * 
	 * @return {Promise}
	 */
	let createBehatYml = () => {
		let cb = yeoman.async();
		yeoman.fs.copyTpl(
			yeoman.templatePath('b2bserver/tests/behat.yml'),
			yeoman.destinationPath(yeoman.userInputs.b2bserver.folder + '/tests/behat.yml'),
			{
				baseUrl: (yeoman.userInputs.b2bserver.https ? 'https://' : 'http://')  + yeoman.userInputs.b2bserver.baseUrl + '/',
			}
		);
		return cb();
	}

	/**
	 * Create woohoo custom configuration file
	 * Which contains mongo db connection details
	 * 
	 * @return {Function} cb
	 */
	let createIronXml = () => {
		let cb = yeoman.async();
		yeoman.fs.copyTpl(
			yeoman.templatePath('b2bserver/app/etc/iron.xml'),
			yeoman.destinationPath(yeoman.userInputs.b2bserver.folder + '/app/etc/iron.xml'),
			yeoman.userInputs.b2bserver
		);
		return cb();
	}

	/**
	 * This creates db import file on b2bserver
	 * As we are unable to import mysql using nodejs using php here
	 * 
	 * @return {Function} cb
	 */
	let createDBImportFile = () => {
		let cb = yeoman.async();
		yeoman.fs.copyTpl(
			yeoman.templatePath('b2bserver/db-import.php'),
			yeoman.destinationPath(yeoman.userInputs.b2bserver.folder + '/db-import.php'),
			yeoman.userInputs.b2bserver
		);
		return cb();
	}

	return new Promise((resolve, reject) => {
		if(yeoman.userInputs.b2bserver == false) {
			return resolve();
		}
		
		cloneRepo()
			.then(() => {
				return createDBImportFile();
			})
			.then(() => {
				return createLocalXml();
			})
			.then(() => {
				return createGBConfigXml();
			})
			.then(() => {
				return createIronXml();
			})
			.then(() => {
				return createBehatYml();
			})
			.then(() => {
				return resolve()
			})
			.then(err => {
				return reject(err);
			});
	});

}