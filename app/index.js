const util = require('util'),
	path = require('path'),
	_ = require('lodash'),
	yeoman = require('yeoman-generator');
	
/**
 * Yeoman generator for woohoo
 * 
 * @type {void}
 */
let WoohooGenerator = module.exports = class extends yeoman {
	constructor(args, opts) {
    	super(args, opts);
    	this.log('\nWelcome to Woohoo Installer.\n');
  	}
};

/**
 * Ask for confirmation to continue or stop application installation 
 * 
 * @return {Promise}
 */
WoohooGenerator.prototype._private_promptStopExec = function _private_promptStopExec() {
	let bat = {},
		prompts = [{
			type: 'confirm',
			name: 'onError',
			message: 'Got error while running command, Would you like to continue ?',
			default: true,
		}];

	return new Promise((resolve, reject) => {
		this.prompt(prompts)
			.then(userInputs => {
				if(userInputs.onError) {
					return resolve();
				}
				else {
					return reject(new Error('Stopping installation as you wish to discontinue'));
				}
			})
			.catch(err => {
				return reject(err);
			});
	});
}

/**
 * All application prompts
 * 
 * @return {Function} cb
 */
WoohooGenerator.prototype.prompts = function prompts () {
	let cb = this.async();
	require('./prompt')(this)
		.then(() => {
			let installRequired = false;
			_.forEach(this.userInputs, userInput => {
				if(userInput != false) {
					installRequired = true
				}
			});

			if(installRequired == true) {
				this.log("\n---------------------------------- \
					\n All configuration are set and proceeding with installation \
					\n----------------------------------");
				return cb();
			}
			else {
				process.exit();
			}
			
		})
		.catch(err => {
			this.log("\n---------------------------------- \
				\nError Occurred \
				\n----------------------------------");
			this.log(err);
			process.exit();
		});
}

/**
 * This will create all applications file system
 * 
 * @return {Function} cb
 */
WoohooGenerator.prototype.files = function files() {
	let cb = this.async();
	require('./files')(this)
		.then(() => {
			this.log("\n---------------------------------- \
				\n All application's files system created \
				\n----------------------------------");
			return cb();
		})
		.catch(err => {
			this.log("\n---------------------------------- \
				\nError Occurred \
				\n----------------------------------");
			this.log(err);
			process.exit();
		});
}

/**
 * Installs all applications
 * 
 * @return {Function} cb
 */
WoohooGenerator.prototype.install = function install () {
	let cb = this.async();
	require('./install')(this)
		.then(() => {
			this.log("\n---------------------------------- \
				\n All application are installed \
				\n----------------------------------");
			return cb();
		})
		.catch(err => {
			this.log("\n---------------------------------- \
				\nError Occurred \
				\n----------------------------------");
			this.log(err);
			process.exit();
		});
}