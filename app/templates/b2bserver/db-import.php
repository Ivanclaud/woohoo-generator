<?php 
class Import {
	/**
	 * @var string
	 */
	private $host = '<%= mysqlHost %>';

	/**
	 * @var string
	 */
	private $username = '<%= mysqlUser %>';

	/**
	 * @var string
	 */
	private $password = '<%= mysqlPassword %>';

	/**
	 * @var string
	 */
	private $dbname = '<%= mysqlDbname %>';

	/**
	 * Imports sql file to database
	 *
	 * @param string $type create|data
	 *
	 * @return $this
	 */
	public function importDb($type) {
		$host = $this->host;
		$username = $this->username;
		$password = $this->password;
		$dbname = $this->dbname;

		$tables = glob(dirname(__FILE__) . "/tests/db/app/**/*." . $type . ".sql");
		foreach ($tables as $table) {
			echo "Importing from " . $table . "\n";
			if($password) {
				exec("mysql -h $host -u $username -p{$password} $dbname < $table");
			}
			else {
				exec("mysql -h $host -u $username $dbname < $table");	
			}
		}

		return $this;
	}
}

$import = new Import();
$import
	->importDb('create')
	->importDb('data');