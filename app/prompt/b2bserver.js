/**
 * @type {Array}
 */
const prompts = [
	{
		type    : 'input',
		name    : 'folder',
		message : 'Installation folder',
		default : 'b2b-magento-sever'
	},
	{
		type    : 'input',
		name    : 'bitbucketUsername',
		message : 'Bit bucket user name',
		default : 'ivanclaud'
	},
	{
		type	: 'input',
		name	: 'branch',
		message	: 'Branch to clone',
		default : 'feature/aws-migration'
	},
	{
		type	: 'input',
		name	: 'baseUrl',
		message	: 'Hosting base url without http(s)://',
		default : 'server.b2b.com'
	},
	{
		type: 'confirm',
		name: 'https',
		message: 'Site will run in HTTPS',
		default: false,
	},
	{
		type	: 'input',
		name	: 'adminUser',
		message	: 'Admin username',
		default : 'admin'
	},
	{
		type	: 'password',
		name	: 'adminPassword',
		message	: 'Admin password',
		default : 'qwik3ilver'
	},
	{
		type	: 'input',
		name	: 'mysqlHost',
		message	: 'Mysql host',
		default : '127.0.0.1'
	},
	{
		type	: 'input',
		name	: 'mysqlUser',
		message	: 'Mysql user',
		default : 'root'
	},
	{
		type	: 'password',
		name	: 'mysqlPassword',
		message	: 'Mysql Password',
		default : ''
	},
	{
		type	: 'input',
		name	: 'mysqlDbname',
		message	: 'Mysql Database name',
		default : 'b2b-magento-server-yo'
	},
	{
		type	: 'input',
		name	: 'mongoHost',
		message	: 'MongoDB host',
		default : '127.0.0.1'
	},
	{
		type	: 'input',
		name	: 'mongoPort',
		message	: 'MongoDB Port',
		default : '27017'
	},
	{
		type	: 'input',
		name	: 'mongoDbname',
		message	: 'MongoDB Database name',
		default : 'b2b-magento-server'
	},
	{
		type	: 'input',
		name	: 'sendgridUsername',
		message	: 'Sendgrid username',
		default : 'giftbigdev'
	},
	{
		type	: 'password',
		name	: 'sendgridPassword',
		message	: 'Sendgrid password',
		default : 'giftbigGBdev'
	},
	{
		type	: 'input',
		name	: 'ironProjectId',
		message	: 'Iron.io Project Id',
		default : '5aa6c7a66e3fde000c9419cf'
	},
	{
		type	: 'password',
		name	: 'ironToken',
		message	: 'Iron.io token',
		default : 'iP-Zubn30Z2adFfg_gXDLqtL7kI'
	},
	{
		type	: 'input',
		name	: 'ironQueuePrefix',
		message	: 'Iron.io queue prefix',
		default : ''
	},
	{
		type	: 'password',
		name	: 'ironMQKey',
		message	: 'Iron.io MQ key for encryption/decryption',
		default : 'KEYFORIRON'
	},
	{
		type	: 'input',
		name	: 'test0CardAPITerminal',
		message	: 'TEST0 cardp api terminal id, without MACID(0.0.0.0)',
		default : 'woohoo:DEV:IVAN:'
	}
];

/**
 * Prompts all B2B server application configuration
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	return new Promise((resolve, reject) => {
		if(yeoman.userInputs.b2bserver == false) {
			yeoman.log('Skipping B2B Server installation');
			return resolve();
		}

		yeoman.log('Here are the few questions of B2B Server application installer');

		yeoman.prompt(prompts)
			.then(userInputs => {
				yeoman.userInputs.b2bserver = userInputs;
				return resolve();
			})
			.catch(err => {
				return resolve(err);
			});
	});
}