/**
 * @type {Array}
 */
const prompts = [
	{
		type    : 'input',
		name    : 'folder',
		message : 'Installation folder',
		default : 'b2b-web-ui'
	},
	{
		type    : 'input',
		name    : 'bitbucketUsername',
		message : 'Bit bucket user name',
		default : 'ivanclaud'
	},
	{
		type	: 'input',
		name	: 'branch',
		message	: 'Branch to clone',
		default : 'woohoo'
	},
	{
		type	: 'input',
		name	: 'baseUrl',
		message	: 'Hosting base url without http(s)://',
		default : 'corporate.b2b.com'
	},
	{
		type: 'confirm',
		name: 'https',
		message: 'Site will run in HTTPS',
		default: false,
	},
	{
		type	: 'input',
		name	: 'serverUrl',
		message	: 'Server URL without http(s)://',
		default : 'sandbox.woohoo.in'
	},
	{
		type	: 'input',
		name	: 'settingsId',
		message	: 'Settings ID',
		default : '561b6880652d39ee5cc82274'
	}
];

/**
 * Prompts all B2B Web UI application configuration
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	return new Promise((resolve, reject) => {
		if(yeoman.userInputs.b2bwebui == false) {
			yeoman.log('Skipping B2B Web UI installation');
			return resolve();
		}

		yeoman.log('Here are the few questions of B2B Web UI application installer');

		yeoman.prompt(prompts)
			.then(userInputs => {
				yeoman.userInputs.b2bwebui = userInputs;
				return resolve();
			})
			.catch(err => {
				return resolve(err);
			});
	});
}