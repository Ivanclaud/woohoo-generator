/**
 * @type {Array}
 */
const prompts = [
	{
		type: 'confirm',
		name: 'b2bserver',
		message: 'Would you like to install Woohoo B2B Server?',
		default: true,
	},
	{
		type: 'confirm',
		name: 'b2bwebui',
		message: 'Would you like to install Woohoo B2B WEB UI?',
		default: true,
	}
];

/**
 * Prompts all application configuration
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	return new Promise((resolve, reject) => {
		yeoman.prompt(prompts)
			.then(userInputs => {
				yeoman.userInputs = userInputs;
				return require('./prompt/b2bserver')(yeoman);
			})
			.then(() => {
				return require('./prompt/b2bwebui')(yeoman);
			})
			.then(() => {
				return resolve();
			})
			.catch(err => {
				return reject(err);
			});
	});
};