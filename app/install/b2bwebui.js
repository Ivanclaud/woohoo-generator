/**
 * Installs woohoo B2B Web UI application
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	/**
	 * Install Dependencies using npm
	 * 
	 * @return {Promise}
	 */
	let installDependencies = () => {
		bat = yeoman.spawnCommandSync('npm', [
			'install'
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	let installGruntCli = () => {
		bat = yeoman.spawnCommandSync('npm', [
			'install',
			'grunt-cli',
			'-g'
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	let buildJs = () => {
		bat = yeoman.spawnCommandSync('grunt');

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	return new Promise((resolve, reject) => {
		if(yeoman.userInputs.b2bwebui == false) {
			return resolve();
		}
		process.chdir(yeoman.userInputs.b2bwebui.folder);

		Promise.resolve()
			.then(() => {
				return installDependencies();
			})
			.then(() => {
				return installGruntCli();
			})
			.then(() => {
				return buildJs();
			})
			.then(() => {
				process.chdir('..');
				return resolve();
			})
			.catch(err => {
				return reject(err);
			});
	});
}