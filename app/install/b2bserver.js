const _ = require('lodash'),
	async = require('async'),
	mysql = require('promise-mysql'),
	request = require('request'),
	fs = require('fs'),
	targz = require('targz'),
	crypto = require('crypto');

/**
 * Installs woohoo B2B server application
 * 
 * @param  {yeoman} yeoman
 * 
 * @return {Promise}
 */
module.exports = (yeoman) => {
	/**
	 * Install Dependencies using composer
	 * 
	 * @return {Promise}
	 */
	let installDependencies = () => {
		bat = yeoman.spawnCommandSync('composer', [
			'install',
			'-d=' + yeoman.userInputs.b2bserver.folder
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	let addSvClient = () => {

		let download = () => {
			return new Promise((resolve, reject) => {
				request({uri: 'http://gbdev.s3.amazonaws.com/SVClient/PHP/4.0/SVClient.tar.gz'})
					.pipe(fs.createWriteStream('SVClient.tar.gz'))
					.on('error', err => {
						return reject(err);
					})
					.on('close', () => {
						return resolve();
					});
			});
		}

		let extract = () => {
			return new Promise((resolve, reject) => {
				let options = {
					src: 'SVClient.tar.gz',
					dest: yeoman.userInputs.b2bserver.folder + '/lib/'
				}
				targz.decompress(options, (err) => {
					if(err) {
						return reject(err);
					}
					else {
						return resolve();
					}
				});
			})
		}


		return new Promise((resolve, reject) => {
			download()
				.then(() => {
					return extract();
				})
				.then(() => {
					fs.unlinkSync('SVClient.tar.gz')
					return resolve();
				})
				.catch(err => {
					return reject(err);
				});
		});
	}

	/**
	 * Creates woohoo B2B server application database
	 * 
	 * @return {Promise}
	 */
	let createDB = () => {
		let opts = [
				'-h',
				yeoman.userInputs.b2bserver.mysqlHost,
				'-u',
				yeoman.userInputs.b2bserver.mysqlUser
			],
			bat;

		if(yeoman.userInputs.b2bserver.mysqlPassword) {
			opts.push('-p' + yeoman.userInputs.b2bserver.mysqlPassword)
		};

		bat = yeoman.spawnCommandSync('mysql', _.concat(opts, [
			'-e create database `' + yeoman.userInputs.b2bserver.mysqlDbname + '`'
		]));

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Imports mysql db using PHP script
	 * 
	 * @return {Promise}
	 */
	let importDB = () => {
		bat = yeoman.spawnCommandSync('php', [
			yeoman.userInputs.b2bserver.folder + '/db-import.php',
		]);
		fs.unlinkSync(yeoman.userInputs.b2bserver.folder + '/db-import.php');

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Connect mysql using provided connection details
	 * 
	 * @return {Promise} connection
	 */
	let connectMysql = () => {
		yeoman.mysql = {};
		yeoman.mysql.pool = mysql.createPool({
	        host: yeoman.userInputs.b2bserver.mysqlHost,
	        user: yeoman.userInputs.b2bserver.mysqlUser,
	        password: yeoman.userInputs.b2bserver.mysqlPassword,
	        database: yeoman.userInputs.b2bserver.mysqlDbname
	    });
	    return yeoman.mysql.pool.getConnection();
	}

	/**
	 * Updates secure and un-secure base url
	 * 
	 * @return {Promise}
	 */
	let updateBaseUrls = () => {
		return new Promise((resolve, reject) => {
			yeoman.mysql.connection.query('update core_config_data set value = ? where path = ? and scope = ?', [
				'http://' + yeoman.userInputs.b2bserver.baseUrl + '/',
				'web/unsecure/base_url',
				'default'
			])
			.then(() => {
				yeoman.log('Updated unsecure base url');
				return yeoman.mysql.connection.query('update core_config_data set value = ? where path = ? and scope = ?', [
					(yeoman.userInputs.b2bserver.https ? 'https://' : 'http://')  + yeoman.userInputs.b2bserver.baseUrl + '/',
					'web/secure/base_url',
					'default'
				])
			})
			.then(() => {
				yeoman.log('Updated secure base url');
				return resolve();
			})
			.catch(err => {
				return reject(err);
			});
		});
	}

	/**
	 * Update admin user name and password
	 * 
	 * @return Promise
	 */
	let updateAdminUser = () => {
		return yeoman.mysql.connection.query('UPDATE `admin_user` SET `username`=?, `password`=?, `is_active`=1 WHERE `user_id`=1', [
			yeoman.userInputs.b2bserver.adminUser,
			crypto.createHash('md5').update(yeoman.userInputs.b2bserver.adminPassword).digest("hex")
		]);
	}

	let updateCardApiTerminal = () => {
		return yeoman.mysql.connection.query('UPDATE cardapi SET `terminalid`=? WHERE `cardapi_id`=1', [
			yeoman.userInputs.b2bserver.test0CardAPITerminal
		]);	
	}

	/**
	 * Apply DB updates
	 * 
	 * @return {Promise}
	 */
	let applyUpdates = () => {
		bat = yeoman.spawnCommandSync('php', [
			yeoman.userInputs.b2bserver.folder + '/shell/apply_db_updates.php'
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * FLush magento cache
	 * 
	 * @return {Promise}
	 */
	let flushCache = () => {
		bat = yeoman.spawnCommandSync('php', [
			yeoman.userInputs.b2bserver.folder + '/shell/flush_cache.php'
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Setup test env using composer script
	 * 
	 * @return {Promise}
	 */
	let testSetup = () => {
		bat = yeoman.spawnCommandSync('composer', [
			'run',
			'setup-test',
			'-d=' + yeoman.userInputs.b2bserver.folder
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Execute units tests
	 * 
	 * @return {Promise}
	 */
	let runUnitTests = () => {
		bat = yeoman.spawnCommandSync('composer', [
			'run',
			'unit-test',
			'-d=' + yeoman.userInputs.b2bserver.folder
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}

	/**
	 * Execute API tests
	 * 
	 * @return {Promise}
	 */
	let runApiTests = () => {
		bat = yeoman.spawnCommandSync('composer', [
			'run',
			'api-test',
			'-d=' + yeoman.userInputs.b2bserver.folder
		]);

		if(bat.status == 0) {
			return Promise.resolve();
		}
		else {
			return yeoman._private_promptStopExec();
		}
	}


	return new Promise((resolve, reject) => {
		if(yeoman.userInputs.b2bserver == false) {
			return resolve();
		}

		Promise.resolve()
			.then(() => {
				return installDependencies();
			})
			.then(() => {
				return addSvClient();
			})
			.then(() => {
				return createDB()
			})
			.then(() => {
				return importDB()
			})
			.then(() => {
				return connectMysql();
			})
			.then(connection => {
				yeoman.mysql.connection = connection;
				return updateBaseUrls();
			})
			.then(() => {
				return updateAdminUser();
			})
			.then(() => {
				return updateCardApiTerminal();
			})
			.then(() => {
				return flushCache();
			})
			.then(() => {
				return applyUpdates();
			})
			.then(() => {
				return flushCache();
			})
			.then(() => {
				return testSetup();
			})
			.then(() => {
				return runUnitTests();
			})
			.then(() => {
				return runApiTests();
			})
			.then(() => {
				yeoman.mysql.pool.end();
				return resolve();
			})
			.catch(err => {
				return reject(err);
			});
	});
}